package cz.gopas.android2.sensors

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener2
import android.hardware.SensorManager
import android.util.Log
import androidx.fragment.app.Fragment
import cz.gopas.android2.R
import kotlinx.android.synthetic.main.fragment_coroutines.*

class SensorsFragment: Fragment(R.layout.fragment_coroutines) {
    private val sensorsManager by lazy { requireContext().getSystemService(Context.SENSOR_SERVICE) as SensorManager }

    private val listener = object: SensorEventListener2 {
        override fun onSensorChanged(event: SensorEvent) {
            Log.v(TAG, "${event.values.asList()}")
            result.text = "${event.values[0]}"
        }

        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
            Log.d(TAG, "Accuracy changed $sensor $accuracy")
        }

        override fun onFlushCompleted(sensor: Sensor) {
            Log.d(TAG, "$sensor flushed")
        }
    }

    override fun onStart() {
        super.onStart()
        sensorsManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)?.let { sensor ->
            sensorsManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI)
        } ?: Log.w(TAG, "No sensor")
    }

    override fun onStop() {
        sensorsManager.unregisterListener(listener)
        super.onStop()
    }

    private companion object {
        private val TAG = SensorsFragment::class.simpleName
    }
}