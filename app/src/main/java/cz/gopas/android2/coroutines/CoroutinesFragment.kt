package cz.gopas.android2.coroutines

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import cz.gopas.android2.MainActivity
import cz.gopas.android2.R
import kotlinx.android.synthetic.main.fragment_coroutines.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CoroutinesFragment : Fragment(R.layout.fragment_coroutines) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        button.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                result.text = "Downloading"
                Log.d(TAG, "${Thread.currentThread().name} 1")
                withContext(IO) {
                    Log.d(TAG, "${Thread.currentThread().name} 2")
                    doSomeNetworking()
                    Log.d(TAG, "${Thread.currentThread().name} 7")
                }
                result.text = "Done"
                Log.d(TAG, "${Thread.currentThread().name} 8")
            }
            Log.d(TAG, "${Thread.currentThread().name} 0")
        }
    }

    private suspend fun doSomeNetworking() {
        Log.d(TAG, "${Thread.currentThread().name} 3")
        withContext(Dispatchers.Main) {
            result.text = "Downloading"
            Log.d(TAG, "${Thread.currentThread().name} 4")
        }
        Log.d(TAG, "${Thread.currentThread().name} 5")
        Thread.sleep(3000)
        Log.d(TAG, "${Thread.currentThread().name} 6")
    }

    companion object {
        val TAG = CoroutinesFragment::class.simpleName
    }
}