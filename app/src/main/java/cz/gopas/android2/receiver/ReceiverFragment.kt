package cz.gopas.android2.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.util.Log
import androidx.fragment.app.Fragment
import cz.gopas.android2.R
import kotlinx.android.synthetic.main.fragment_coroutines.*

class ReceiverFragment: Fragment(R.layout.fragment_coroutines) {
    private val batteryReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d(TAG, "${intent.extras?.keySet()?.toList()}")
            val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
            val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 1)
            result.text = "${level * 100 / scale.toFloat()}"
        }
    }

    override fun onStart() {
        super.onStart()
        requireContext().registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    }

    override fun onStop() {
        requireContext().unregisterReceiver(batteryReceiver)
        super.onStop()
    }

    private companion object {
        private val TAG = ReceiverFragment::class.simpleName
    }
}