package cz.gopas.android2

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class GopasFirebaseService: FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        Log.i(TAG, "New token: $token")
    }

    override fun onMessageReceived(msg: RemoteMessage) {
        Log.d(TAG, "New message: ${msg.data}")
    }

    private companion object {
        private val TAG = GopasFirebaseService::class.simpleName
    }
}