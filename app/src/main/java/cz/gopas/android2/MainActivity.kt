package cz.gopas.android2

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.google.firebase.messaging.FirebaseMessaging
import cz.gopas.android2.coroutines.CoroutinesFragment
import cz.gopas.android2.location.LocationFragment
import cz.gopas.android2.maps.MapsFragment
import cz.gopas.android2.receiver.ReceiverFragment
import cz.gopas.android2.retrofit.RetrofitFragment
import cz.gopas.android2.sensors.SensorsFragment
import cz.gopas.android2.service.ServiceFragment
import cz.gopas.android2.web.WebFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class MainActivity : AppCompatActivity() {
    private val drawerToogle by lazy {
        object : ActionBarDrawerToggle(
            this,
            drawer_layout,
            R.string.drawer_open,
            R.string.drawer_close
        ) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                invalidateOptionsMenu()
            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                invalidateOptionsMenu()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        drawer_layout.addDrawerListener(drawerToogle)

        with(supportActionBar!!) {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }

        drawer.setNavigationItemSelectedListener {
            supportFragmentManager.commit {
                when (it.itemId) {
                    R.id.coroutines -> replace(R.id.fragments, CoroutinesFragment())
                    R.id.retrofit -> replace(R.id.fragments, RetrofitFragment())
                    R.id.maps -> replace(R.id.fragments, MapsFragment())
                    R.id.location -> replace(R.id.fragments, LocationFragment())
                    R.id.sensors -> replace(R.id.fragments, SensorsFragment())
                    R.id.web -> replace(R.id.fragments, WebFragment())
                    R.id.service -> replace(R.id.fragments, ServiceFragment())
                    R.id.receiver -> replace(R.id.fragments, ReceiverFragment())
                    else -> Log.w(TAG, "Unknown item")
                }
            }
            drawer_layout.closeDrawer(drawer)
            true
        }

        lifecycleScope.launch {
            Log.i(TAG, "Firebase token: ${FirebaseMessaging.getInstance().token.await()}")
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToogle.syncState()
    }

    override fun onOptionsItemSelected(item: MenuItem) = drawerToogle.onOptionsItemSelected(item)

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToogle.onConfigurationChanged(newConfig)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(drawer)) {
            drawer_layout.closeDrawer(drawer)
        } else {
            super.onBackPressed()
        }
    }

    private companion object {
        private val TAG = MainActivity::class.simpleName
    }
}
