package cz.gopas.android2.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.*
import cz.gopas.android2.R
import kotlinx.android.synthetic.main.fragment_coroutines.*

class LocationFragment: Fragment(R.layout.fragment_coroutines) {

    private val fusedLocationClient by lazy { LocationServices.getFusedLocationProviderClient(requireContext()) }

    private val locationCallback = object: LocationCallback(){
        override fun onLocationResult(loc: LocationResult) {
            Log.d(TAG, "$loc")
            result.text = "${loc.locations[0].latitude} ${loc.locations[0].longitude}"
        }

        override fun onLocationAvailability(availability: LocationAvailability) {
            Log.d(TAG, "$availability")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            Log.d(TAG, "Permission denied")
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
                if (granted) {
                    showLocation()
                } else {
                    TODO("Explain to user and ask again")
                }
            }.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        } else {
            Log.d(TAG, "Permission granted")
            showLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun showLocation() = fusedLocationClient.requestLocationUpdates(LocationRequest(), locationCallback, null)

    override fun onStop() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
        super.onStop()
    }

    private companion object {
        private val TAG = LocationFragment::class.simpleName
    }
}