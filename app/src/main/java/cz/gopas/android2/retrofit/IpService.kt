package cz.gopas.android2.retrofit

import retrofit2.http.GET

interface IpService {
    @GET("/ip")
    suspend fun ip(): IpModel
}