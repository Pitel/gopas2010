package cz.gopas.android2.retrofit

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.chuckerteam.chucker.api.ChuckerInterceptor
import cz.gopas.android2.R
import kotlinx.android.synthetic.main.fragment_coroutines.*
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitFragment: Fragment(R.layout.fragment_coroutines) {
    private val okHttp by lazy {
        OkHttpClient.Builder()
            .addNetworkInterceptor(ChuckerInterceptor(requireContext()))
            .build()
    }

    private val rest by lazy {
        Retrofit.Builder()
            .baseUrl("https://httpbin.org")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttp)
            .build()
            .create(IpService::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        button.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                result.text = rest.ip().origin
            }
        }
    }
}