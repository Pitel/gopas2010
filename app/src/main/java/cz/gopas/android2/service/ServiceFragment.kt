package cz.gopas.android2.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import cz.gopas.android2.R
import kotlinx.android.synthetic.main.fragment_coroutines.*

class ServiceFragment: Fragment(R.layout.fragment_coroutines) {
    var service: GopasService? = null

    val connection = object: ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder) {
            service = (binder as GopasService.GopasBinder).service
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            service = null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        button.setOnClickListener {
            result.text = service?.greet("Honza")
        }
    }

    override fun onStart() {
        super.onStart()
        ContextCompat.startForegroundService(requireContext(), Intent(requireContext(), GopasService::class.java))
        requireContext().bindService(Intent(requireContext(), GopasService::class.java), connection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        requireContext().unbindService(connection)
        requireContext().stopService(Intent(requireContext(), GopasService::class.java))
        service = null
        super.onStop()
    }
}