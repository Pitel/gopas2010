package cz.gopas.android2.service

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.util.Log
import androidx.core.app.NotificationCompat
import cz.gopas.android2.MainActivity

class GopasService : Service() {
    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "Created")
        startForeground(
            54,
            NotificationCompat.Builder(this, "")
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentTitle("Title")
                .setContentText("Text")
                .setContentIntent(
                    PendingIntent.getActivity(
                        this,
                        84,
                        Intent(this, MainActivity::class.java),
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )
                )
                .setAutoCancel(true)
                .build()
        )
    }

    override fun onBind(intent: Intent?) = GopasBinder()

    fun greet(name: String) = "Hello $name!"

    override fun onDestroy() {
        Log.d(TAG, "Destroyed")
        super.onDestroy()
    }

    inner class GopasBinder : Binder() {
        val service = this@GopasService
    }

    private companion object {
        private val TAG = GopasService::class.simpleName
    }
}