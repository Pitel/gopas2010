package cz.gopas.android2.web

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.widget.Toast
import androidx.fragment.app.Fragment

class WebFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = WebView(requireContext())

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with (view as WebView) {
            settings.javaScriptEnabled = true
            loadUrl("file:///android_asset/index.html")
            addJavascriptInterface(JSInterface(), "Android")
        }
    }

    private inner class JSInterface {
        @JavascriptInterface
        fun showToast(toast: String) {
            Toast.makeText(requireContext(), toast, Toast.LENGTH_SHORT).show()
        }
    }
}